package com.breakly.reveals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RevealsServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RevealsServerApplication.class, args);
	}

}
