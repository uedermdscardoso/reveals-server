package com.breakly.reveals.web.controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.breakly.reveals.web.domain.model.caption.CaptionDTO;
import com.breakly.reveals.web.domain.model.language.Language;
import com.breakly.reveals.web.services.WatsonService;
import com.ibm.watson.speech_to_text.v1.model.SpeechRecognitionAlternative;
import com.ibm.watson.speech_to_text.v1.model.SpeechRecognitionResult;

//https://manojbhor.medium.com/nsfw-classifier-nudity-classification-on-mobile-and-edge-devices-bae14c171a89

@Controller
@RequestMapping("/recognize")
public class SocketController {

	//@Autowired
    //SimpMessagingTemplate template;

    @Autowired
    WatsonService watsonService;
    
    @Autowired
    private ResourceLoader resourceLoader;

    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(10);

    @GetMapping("/ok")
    public ResponseEntity<String> teste() {
    	return ResponseEntity.ok("CERTO");
    }
    
    //@PostMapping("/media/{language}")
    @RequestMapping(path = "/media/{language}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CaptionDTO> sendMessage(@PathVariable String language, @RequestParam MultipartFile audio) throws Exception {
        
		try {
			
			if(Language.valueOf(language.toUpperCase()).name().isEmpty())
				throw new Exception("Language not found");
			
			final UUID id = UUID.randomUUID();
			FileUtils.writeByteArrayToFile(new File("temp_file_"+id), audio.getBytes());
			final File media = new File("temp_file_"+id);
			
			final CaptionDTO captionDTO = watsonService.recognizeAudio(Language.valueOf(language.toUpperCase()), media);
		
			return ResponseEntity.ok(captionDTO);
		} catch (IOException e) {
			return ResponseEntity.badRequest().build();
		} catch(Exception e) {
			return ResponseEntity.badRequest().build();	
		}
    }
    
    /*@MessageMapping("/hello")
    public void greeting() {
        
    	final Resource fileResource = resourceLoader.getResource("classpath:8TzgE5KgzVXVRElsLSwd.flac");
    	
    	File audio;
    	
		try {
			audio = fileResource.getFile();
	    	
			scheduler.scheduleAtFixedRate(() -> {
	            template.convertAndSend("/topic/message", watsonService.recognizeAudio(audio));
	        }, 0, 2, TimeUnit.SECONDS);	    	
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	/*scheduler.scheduleAtFixedRate(() -> {
            template.convertAndSend("/topic/message", watsonService.getMessage());
        }, 0, 2, TimeUnit.SECONDS);
        
    }*/
	
}
