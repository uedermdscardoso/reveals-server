package com.breakly.reveals.web.domain.model.caption;

import java.util.List;
import java.util.Map;

public class CaptionDTO {

	private List<Map<String, String>> content;
	
	public CaptionDTO() {
		
	}
	
	public CaptionDTO(List<Map<String, String>> content) {
		this.content = content;
	}
	
	public List<Map<String, String>> getContent() {
		return this.content;
	}
	
	public void setContent(List<Map<String, String>> content) {
		this.content = content;
	}
	
}
