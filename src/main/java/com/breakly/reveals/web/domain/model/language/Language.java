package com.breakly.reveals.web.domain.model.language;

public enum Language {
	PT_BR,
	EN_US,
	ES_ES
}
